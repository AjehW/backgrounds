from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    name = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField()


    def __str__(self):
        return self.name
