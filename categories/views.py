from django.shortcuts import render, get_object_or_404, redirect
from categories.models import Category
from django.contrib.auth.decorators import login_required
from categories.forms import CategoryForm



def list_categories(request):
    categories = Category.objects.all()
    context = {"list_categories": categories}
    return render(request, "categories/list.html", context)



def show_category(request, id):
    category = get_object_or_404(Category, id=id)
    context = {"show_category": category}
    return render(request, "categories/detail.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.save()
            return redirect("list_categories")
    else:
        form = CategoryForm()
    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)

