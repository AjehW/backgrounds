from django.urls import path
from categories.views import list_categories, show_category, create_category


urlpatterns = [
    path("", list_categories, name="list_categories"),
    path("<int:id>/", show_category, name="show_category"),
    path("create/", create_category, name="create_category"),

]
