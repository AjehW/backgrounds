from django.urls import path
from images.views import add_background, my_backgrounds, show_background, edit_background, my_backgrounds_delete


urlpatterns = [
    path("add_background/", add_background, name="add_background"),
    path("my_backgrounds/", my_backgrounds, name="my_backgrounds"),
    path("<int:id>/", show_background, name="show_background"),
    path("<int:id>/edit/", edit_background, name="edit_background"),
    path("<int:id>/delete/", my_backgrounds_delete, name="my_backgrounds_delete"),
]
