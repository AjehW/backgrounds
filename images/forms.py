from django.forms import ModelForm
from images.models import Background


class BackgroundForm(ModelForm):
    class Meta:
        model = Background
        fields = "__all__"
