from django.contrib import admin
from images.models import Background


@admin.register(Background)
class BackgroundAdmin(admin.ModelAdmin):
    list_display = (
       "title",
        "id",
        "creator"
    )
