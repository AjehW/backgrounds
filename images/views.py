from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from images.forms import BackgroundForm
from images.models import Background

def show_background(request, id):
    background = get_object_or_404(Background, id=id)
    context = {
        "background_object": background,
    }
    return render(request, "images/background_detail.html", context)

@login_required
def add_background(request):
    if request.method == "POST":
        form = BackgroundForm(request.POST)
        if form.is_valid():
            background = form.save(False)
            background.creator = request.user
            background.save()
            return redirect("my_backgrounds")
    else:
        form = BackgroundForm()
    context = {
        "form": form,
    }
    return render(request, "images/add_background.html", context)


@login_required
def my_backgrounds(request):
    backgrounds = Background.objects.filter(creator=request.user)
    context = {"my_backgrounds": backgrounds}
    return render(request, "images/my_backgrounds.html", context)


@login_required
def edit_background(request, id):
    background = get_object_or_404(Background, id=id)
    if request.method == "POST":
        form = BackgroundForm(request.POST, instance=background)
        if form.is_valid():
            form.save()
            return redirect("show_background", id=id)
    else:
        form = BackgroundForm(instance=background)

    context = {
        "background_object": background,
        "form": form,
    }
    return render(request, "images/edit_background.html", context)


@login_required
def my_backgrounds_delete(request, id):
    background = get_object_or_404(Background, id=id)
    if request.method == "POST":
        background.delete()
        return redirect("my_backgrounds")
    return render(request, "images/delete.html")
