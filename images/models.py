from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from categories.models import Category


class Background(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField(max_length=250)
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    creator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="images",
        on_delete=models.CASCADE,
        null=True,
    )
    category = models.ForeignKey(
        Category,
        related_name="images",
        on_delete=models.CASCADE,
        default=True
    )

    def __str__(self):
        return self.title
